#!/bin/bash
#export LD_LIBRARY:PATH=/usr/local/lib
gnunet-arm -s -c /conf/gnunet.conf -L EVERYTHING -l /logs/arm.log #> $HOME/gnunet.log 2>&1
#Listen on all interfaces in docker
gnunet-config -s rest -o BIND_TO -V "0.0.0.0"
gnunet-config -s rest -o BIND_TO6 -V "::"
#Restart
gnunet-arm -r -c /conf/gnunet.conf -L EVERYTHING -l /logs/arm.log
exec bash
