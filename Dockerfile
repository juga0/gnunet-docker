FROM debian:buster-slim

RUN apt update
RUN apt upgrade -y

RUN apt install -y git libtool autoconf autopoint \
build-essential libgcrypt-dev libidn11-dev zlib1g-dev \
libunistring-dev libglpk-dev miniupnpc libextractor-dev \
libjansson-dev libcurl4-gnutls-dev gnutls-bin libsqlite3-dev \
openssl libnss3-tools libopus-dev libpulse-dev libogg-dev \ 
libargon2-dev libsodium-dev libgnutls28-dev htop wget \
python3-pip nano nmap

RUN pip3 install requests

WORKDIR /
RUN echo "building libmicrohttpd"
RUN /usr/bin/wget https://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-0.9.71.tar.gz
RUN /bin/tar -xf /libmicrohttpd-0.9.71.tar.gz
RUN /bin/rm /libmicrohttpd-0.9.71.tar.gz

WORKDIR /libmicrohttpd-0.9.71
RUN autoreconf -fi
RUN ./configure --disable-doc --prefix=/opt/libmicrohttpd
RUN make -j$(nproc || echo -n 1)
RUN make install

WORKDIR /
RUN echo "building gnunet"
RUN /usr/bin/wget http://gnu.askapache.com/gnunet/gnunet-0.13.2.tar.gz
RUN /bin/tar -xf /gnunet-0.13.2.tar.gz
RUN /bin/rm gnunet-0.13.2.tar.gz

WORKDIR /gnunet-0.13.2
ENV GNUNET_PREFIX=/usr/local
ENV CFLAGS="-g -Wall -O0"
RUN ./configure --prefix=$GNUNET_PREFIX --disable-documentation --enable-logging=verbose --with-microhttpd=/opt/libmicrohttpd
RUN /usr/sbin/addgroup gnunet
RUN /usr/sbin/addgroup gnunetdns
RUN /usr/sbin/adduser --system --home /var/lib/gnunet gnunet
RUN /usr/sbin/usermod -aG gnunet root
RUN make -j$(nproc || echo -n 1)
RUN make install

WORKDIR /
RUN mkdir -p ~/.config
RUN touch ~/.config/gnunet.conf
RUN mkdir -p /logs
RUN touch /logs/arm.log

ENV LD_LIBRARY_PATH=/usr/local/lib

#EXPOSE 7777

COPY docker-entrypoint.sh /opt

ENV PATH="/usr/local/share/bin:$PATH"

ENTRYPOINT ["bash", "/opt/docker-entrypoint.sh"]
#ENTRYPOINT ["/opt/docker-entrypoint.sh"]
#ENTRYPOINT ["/usr/local/bin/gnunet-arm -s", "bash"]
#ENTRYPOINT /usr/local/bin/gnunet-arm -s
#ENTRYPOINT ["bash", "/usr/local/bin/gnunet-arm -s"]
#ENTRYPOINT ["gnunet-arm"]
#CMD ["-s"]
